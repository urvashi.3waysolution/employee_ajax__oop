<?php
include("include/dbconfig.php");
$cnn =new connection();
include("include/session.php");
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Employee</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">    
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <script src="assets/plugins/bootstrap/css/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="ajax.js"></script>
     <style>
    #delete{
        display: block;
  margin-left: auto;
    }
    .is-hide{
        display : none;
    }
    .error{color:#FF0000};
    </style>

</head>
<body>
   <?php include("include/left-right-navbar.php"); ?>

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                            <?php
                            $id=$_SESSION['id'];
                            $UserData=$cnn->getrows("select first_name from registration_master where id='$id'"); 
                            while($firstname_qury=mysqli_fetch_assoc($UserData))
                            {
                                $first_name = $firstname_qury['first_name'];
                             
                            ?>
							<B>Welcome<div class="fluid username"  > <?php echo $first_name; ?></div></B>
                            <?php
        }
    ?>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="content">
        <div class="animated fadeIn table employeeListing">
        <!-- Small modal -->
        <input type="hidden" class="btn btn-primary clickModel" data-toggle="modal" data-target="#staticModal">

        <button type="button" class="btn btn-primary btn-lg addEmployee">ADD EMPLOYEE</button>
                <div class="row" >
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="col-lg-8">
                                <div class="form-group" >
                                    <!-- <label class=" form-control-label">SEARCH</label> -->
                                    </br>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                        <input class="form-control" type="text" name="search" id="search">
                                    </div>
                                
                                </div>
                            </div>
                            <div class="card-header">
                                <strong class="card-title">Employee Table</strong>
                                <button type="button" class="btn btn-outline-danger" id="delete">Delete</button>
                            </div>
                            <div class="table-stats order-table ov-h" id="viewtable">
                               
                            </div> <!-- /.table-stats -->
                        </div>
                    </div>
            </div>
       
    </div><!-- .animated -->
<div class="animated fadeIn table registerForm is-hide">
<button type="button" class="btn btn-primary btn-lg ViewEmployee">VIEW EMPLOYEE</button>
<div class="row">         
<div class="col-xs-12 col-sm-12">
    <div class="card">
        
        <div class="card-body card-block">
        <form  id="frm" method="POST" action="" enctype="multipart/form-data">
            <div class="form-group">
                <label class=" form-control-label">FIRST NAME</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
                    <input class="form-control" type="text" name="first_name" id="first_name">
                </div>
                <span class="error demofn"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">LAST NAME</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
                    <input class="form-control" type="text" name="last_name" id="last_name">
                </div>
                <span class="error demoln"></span>
            </div>
            <div class="form-group">
              
                    <label class="form-label">GENDER</label>
                </br>   
                        <input type="radio" name="gender" id="male"  class="with-gap gender" value="0" >
                        <label for="male">Male</label>
                        <input type="radio" name="gender" id="female"   class="with-gap gender" value="1" >
                        <label for="female" class="m-l-20">Female</label>
                    <span class="error demogn"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">ADDRESS</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-address-book-o"></i></div>
                    <input class="form-control" type="text" name="address" id="address">
                </div>
                <span class="error demoad"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">BIRTH DATE</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-address-book-o"></i></div>
                    <input class="form-control" type="date" name="date_of_birth" id="date_of_birth" >
                </div>
                <span class="error demobd"></span>
            </div>
           
            <div class="form-group">
                <label class=" form-control-label">MOBILE NO</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                    <input class="form-control" type="text" name="mobile_no" id="mobile_no">
                </div>
                <span class="error demomn"></span>
            </div>
             <div class="form-group">
                <label class=" form-control-label">PROFILE</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-file-picture-o"></i></div>
                    <input type="file"  name="image" id="image" class="form-control" value="">
                   
                </div>
                <span class="error demoim"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">EMAIL</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
                    <input class="form-control" type="email" name="email" id="email">
                </div>
                <span class="error demoem"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">PASSWORD</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-key"></i></div>
                    <input class="form-control" type="password" name="password" id="password" autocomplete="on">
                </div>
                <span class="error demopa"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">CONFIRM PASSWORD</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-key"></i></div>
                    <input class="form-control" type="password" name="confirm_password" id="confirm_password" autocomplete="on">
                </div>
                <span class="error democp"></span>
            </div>
            <div class="form-group">
                <label class=" form-control-label">ROLE</label>
                <div class="input-group">
                   <select name="role" id="role" class="form-control">
                            <option value="" selected disabled>ROLE</option>
                            <option value="0">ADMIN</option>
                            <option value="1">EMPLOYEE</option>
                        </select>
                </div>
                <spam class="error demorl"></spam>
            </div>
            <div class="form-group">
                <label class=" form-control-label">JOIN DATE</label>
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-address-book-o"></i></div>
                    <input class="form-control" type="date" name="joining_date" id="joining_date" >
                    </div>
                    <span class="error demojd"></span>
                </div>
          
            <div class="form-group">
                <div class="input-group">
                <input class="btn btn-success" id ="submit" name="submit" type="submit" value="Submit">
                </div>
            
            </div>
            </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div><!-- .content -->
<div class="clearfix"></div>
</div><!-- /#right-panel -->
<!-- Right Panel -->
<div class="modal " id="modalForm" role="dialog" width="100%"  data-backdrop="">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">UPDATE FORM</h4>
            </div>
            <!-- Modal Body -->
            <div class="modal-body"  id="editdata">
                <p class="statusMsg"></p>
                
                <form id="fupForm" name="form1" method="post" enctype="multipart/form-data">     
                <input type="hidden" name="id" id="id" value=""/>         
                <input type="hidden" name="call_for" class="callFor" value="0"/>    
                <div class="form-group">
    <label class=" form-control-label">First Name</label>
    <div class="input-group">   
        <div class="input-group-addon"><i class="fa fa-user-circle"></i></div>
        <input type="text" class="form-control first_name"   id="first_name1" name="first_name" placeholder="First Name" >   
        </div>
    <span class="error demofn1"> </span>  
</div>  
<div class="form-group">
    <label class=" form-control-label">Last Name</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-user-o"></i></div>
       <input type="text" class="form-control item last_name" id="last_name1" name="last_name" placeholder="Last Name" >
    </div>
    <span class="error demoln1"> </span>
</div>

<div class="form-group">
    <label class=" form-control-label">BIRTH Date</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
       <input type="date" class="form-control item date_of_birth" name="date_of_birth" id="date_of_birth1" placeholder="Date Of Birth" >
    </div>
     <span class="error demobd1"> </span>
</div>
 <div class="form-group">
         <label class=" form-control-label">Gender</label>
        <div class="col-sm-10">
            <input type="radio" name="gender" id="gendermale1"  class="with-gap gender"   value="0">
            <label for="male">Male</label>
            <input type="radio" name="gender" id="genderfemale1"  class="with-gap gender" value="1">
            <label for="female" class="m-l-20">Female</label>
        </div>
         <span class="error demogn1"> </span>
</div>
<div class="form-group">
    <label class=" form-control-label">Address</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-address-book"></i></div>
       <input type="text" class="form-control item address" id="address1" name="address" placeholder="Address" >
    </div>
    <span class="error demoad1"> </span>
</div>
<div class="form-group">
    <label class=" form-control-label">Phone NO</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
       <input type="text" class="form-control item mobile_no" name="mobile_no" id="mobile_no1" placeholder="Mobile No" >
    </div>       
      <span class="error demomn1"> </span>
</div>
<div class="form-group">
    <label class=" form-control-label">Email</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
       <input type="text" class="form-control item email" name="email" id="email1" placeholder="Email" >
    </div>
    <span class="error demoem1"> </span>
    
</div>
<div class="form-group">
    <label class="form-control-label">Password</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
        <input type="password" class="form-control item password" name="password" id="password1" placeholder="Password" autocomplete="on">
    </div> 
    <span class="error demopa1"> </span>
</div>
<div class="form-group">
    <label class="form-control-label">Confirm Password</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
        <input type="password" class="form-control item confirm_password"  name="confirm_password" id="confirm_password1" placeholder="Confirm Password" autocomplete="on">
    </div>
     <span class="error democp1"> </span>
</div>
<div class="form-group">
    <label class=" form-control-label">Image</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-image"></i></div>
       <input type="file" name="image" id="image1" class="form-control image" value="">
       <span class="error demoim1"> </span>
    </div>
   
       <span>
       <img class="updateImage" src="" height="100" width="100">
       </span>
     
</div>
<div class="form-group">
  <label class=" form-control-label">Joining Date</label>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar-o"></i></div>
       <input type="date" class="form-control item joining_date" name="joining_date" id="joining_date1" placeholder="Joining Date" >
    </div>
  <span class="error demojd1"> </span>
</div>
        <div class="row form-group">
        
                <div class="col col-md-3"><label for="select" class=" form-control-label role">Role</label>
                </div>
                <div class="col-12 col-md-12">
                    <select name="role" id="role1" class="form-control show-tick role"  style="width: 100%;" >
                            <option value="" selected disabled>Role</option>
                            <option value="0" disabled>Admin</option>
                            <option value="1" disabled>Employee</option>           
                    </select>
                </div>
                <span class="error demoro1"> </span>
            </div>
            <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-primary update" name="update" value="update" id="update" >UPDATE</button>
                    </div>
            </form>
        </div>            
        </div>
    </div>
</div>

      
       <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
        <div class="modal-header">
          
            <h4 class="modal-title w-100 msg1"></h4>	
        </div>
        <div class="modal-body">
            <p class="text-center msg2"></p>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
        </div>
    </div>
    </div>
</div>
</div>

<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
<script src="assets/js/main.js"></script>
 </body>
</html>
