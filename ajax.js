$(document).ready(function() {
    $(".addEmployee").click(function() {
        $(".employeeListing").hide();
        $(".registerForm").show();
    });
    $(".ViewEmployee").click(function() {
        $(".registerForm").hide();
        $(".employeeListing").show();
    });

    $("#submit").click(function(event) {
        event.preventDefault();
        var form_data = $("#frm")[0];
        var form_data = new FormData(form_data);
        form_data.append("method", "insert");
        $.ajax({
            url: "responce.php",
            type: "POST",
            data: form_data,
            processData: false,
            contentType: false,
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                if (dataResult.dataResult == "fail") {
                    dataResult["errors"]["fn"] !== undefined ? $(".demofn").html(dataResult["errors"]["fn"]) : $(".demofn").html("");
                    dataResult["errors"]["ln"] !== undefined ? $(".demoln").html(dataResult["errors"]["ln"]) : $(".demoln").html("");
                    dataResult["errors"]["bd"] !== undefined ? $(".demobd").html(dataResult["errors"]["bd"]) : $(".demobd").html("");
                    dataResult["errors"]["ad"] !== undefined ? $(".demoad").html(dataResult["errors"]["ad"]) : $(".demoad").html("");
                    dataResult["errors"]["rl"] !== undefined ? $(".demorl").html(dataResult["errors"]["rl"]) : $(".demorl").html("");
                    dataResult["errors"]["gn"] !== undefined ? $(".demogn").html(dataResult["errors"]["gn"]) : $(".demogn").html("");
                    dataResult["errors"]["im"] !== undefined ? $(".demoim").html(dataResult["errors"]["im"]) : $(".demoim").html("");
                    dataResult["errors"]["mn"] !== undefined ? $(".demomn").html(dataResult["errors"]["mn"]) : $(".demomn").html("");
                    dataResult["errors"]["cp"] !== undefined ? $(".democp").html(dataResult["errors"]["cp"]) : $(".democp").html("");
                    dataResult["errors"]["em"] !== undefined ? $(".demoem").html(dataResult["errors"]["em"]) : $(".demoem").html("");
                    dataResult["errors"]["pa"] !== undefined ? $(".demopa").html(dataResult["errors"]["pa"]) : $(".demopa").html("");
                    dataResult["errors"]["jd"] !== undefined ? $(".demojd").html(dataResult["errors"]["jd"]) : $(".demojd").html("");
                } else {
                    $(".demofn").html("");
                    $(".demoln").html("");
                    $(".demobd").html("");
                    $(".demoad").html("");
                    $(".demorl").html("");
                    $(".demogn").html("");
                    $(".demoim").html("");
                    $(".demomn").html("");
                    $(".democp").html("");
                    $(".demoem").html("");
                    $(".demopa").html("");
                    $(".demojd").html("");
                    $("#frm")[0].reset();
                    load_data(1, 1);
                }
            },
        });
    });


    $("#image1").change(function() {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.updateImage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".changeProfile").click(function() {
        $('.callFor').val('1');
        // $('.nameChange').val('1');
    });
    $("#update").click(function(event) {
        event.preventDefault();
        var form_data = $("#fupForm")[0];
        var form_data = new FormData(form_data);
        form_data.append("method", "update");
        $.ajax({
            url: "responce.php",
            type: "post",
            data: form_data,
            processData: false,
            contentType: false,
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                if (dataResult.dataResult == "fail") {
                    dataResult["errors"]["fn"] !== undefined ? $(".demofn1").html(dataResult["errors"]["fn"]) : $(".demofn1").html("");
                    dataResult["errors"]["ln"] !== undefined ? $(".demoln1").html(dataResult["errors"]["ln"]) : $(".demoln1").html("");
                    dataResult["errors"]["bd"] !== undefined ? $(".demobd1").html(dataResult["errors"]["bd"]) : $(".demobd1").html("");
                    dataResult["errors"]["ad"] !== undefined ? $(".demoad1").html(dataResult["errors"]["ad"]) : $(".demoad1").html("");
                    dataResult["errors"]["im"] !== undefined ? $(".demoim1").html(dataResult["errors"]["im"]) : $(".demoim1").html("");
                    dataResult["errors"]["gn"] !== undefined ? $(".demogn1").html(dataResult["errors"]["gn"]) : $(".demogn1").html("");
                    dataResult["errors"]["mn"] !== undefined ? $(".demomn1").html(dataResult["errors"]["mn"]) : $(".demomn1").html("");
                    dataResult["errors"]["cp"] !== undefined ? $(".democp1").html(dataResult["errors"]["cp"]) : $(".democp1").html("");
                    dataResult["errors"]["em"] !== undefined ? $(".demoem1").html(dataResult["errors"]["em"]) : $(".demoem1").html("");
                    dataResult["errors"]["pa"] !== undefined ? $(".demopa1").html(dataResult["errors"]["pa"]) : $(".demopa1").html("");
                    dataResult["errors"]["jd"] !== undefined ? $(".demojd1").html(dataResult["errors"]["jd"]) : $(".demojd1").html("");
                } else {
                    $(".demofn1").html("");
                    $(".demoln1").html("");
                    $(".demobd1").html("");
                    $(".demoad1").html("");
                    $(".demoim1").html("");
                    $(".demogn1").html("");
                    $(".demomn1").html("");
                    $(".democp1").html("");
                    $(".demoem1").html("");
                    $(".demojd1").html("");
                    $(".demopa1").html("");
                    $("#modalForm").modal('hide');
                    $('.modal-backdrop').remove();
                    $('.modal-show').hide();
                    //    success: function(response){
                    if ($('.callFor').val() == '1') {
                        var img_src = $('.updateImage').attr('src');
                        jQuery('.profileImg').attr('src', img_src);
                        var namechange = $('#first_name1').val();
                        $('.username').html(namechange);
                    }
                    load_data(2, 1);

                }
            },
        });
    });

    load_data(0, 1);

    function load_data(call, page, query = "") {

        $.ajax({
            url: "responce.php",
            type: "POST",
            data: { method: "search", page: page, query: query },
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                $("#viewtable").html(dataResult["output"]);
                $(".registerForm").hide();
                $(".employeeListing").show();
                if (call == 1) {
                    $(".clickModel").click();
                    $(".msg1").html("Success");
                    $(".msg2").html("Register successfully");
                } else if (call == 2) {
                    $(".clickModel").click();
                    $(".msg1").html("Update");
                    $(".msg2").html("Update successfully");

                } else {
                    $(".msg1").html();
                    $(".msg2").html();

                }
            },
        });
    }

    $(document).on("click", ".page-link", function() {
        var page = $(this).data("page_number");
        var query = $("#search").val();
        load_data(0, page, query);
    });

    $("#search").keyup(function(event) {
        event.preventDefault();
        var query = $("#search").val();
        load_data(0, 1, query);
    });

    function updatedata(id) {
        $.ajax({
            url: "responce.php",
            type: "POST",
            data: { method: "edit", id: id },
            cache: false,
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                // console.log(dataResult);
                // return false;
                checkIfFileLoaded(dataResult.image);
                $("#id").val(dataResult.id);
                $(".first_name").val(dataResult.first_name);
                $(".last_name").val(dataResult.last_name);
                $(".date_of_birth").val(dataResult.date_of_birth);
                if (dataResult.gender == "0") {
                    $('#gendermale1').val('0').prop("checked", "checked");
                } else {
                    $('#genderfemale1').val('1').prop("checked", "checked");
                }
                $(".address").val(dataResult.address);
                $(".mobile_no").val(dataResult.mobile_no);
                $(".email").val(dataResult.email);
                $(".password").val(dataResult.password);
                $(".role").val(dataResult.role);
                $(".joining_date").val(dataResult.joining_date);
                $(".confirm_password").val(dataResult.confirm_password);
                $("#modalForm").modal("show");
            },
        });
    }
    $(document).on("click", ".edit", function() {
        var el = this;
        var id = $(this).data("id");
        updatedata(id);
    });


    $(document).on("click", ".delete", function(event) {
        event.preventDefault();
        var el = this;
        var id = $(this).data("id");
        var confirmalert = confirm("Are you sure?");
        if (confirmalert == true) {
            $.ajax({
                url: "responce.php",
                type: "POST",
                data: {
                    method: "delete",
                    id: id,
                },
                success: function(dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        $(el).closest("tr").css("background", "tomato");
                        $(el)
                            .closest("tr")
                            .fadeOut(800, function() {
                                $(this).remove();
                            });
                    } else {
                        alert("something went wrong");
                    }
                },
            });
        }
    });
    $(document).on("click", ".updatesatus", function() {
        var el = this;
        var id = $(this).data("id");
        var status = ($(this).hasClass("btn-success")) ? '0' : '1';
        $.ajax({
            type: "POST",
            url: "responce.php",
            data: {
                method: "updatestatus",
                id: id,
                status: status
            },
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                if (dataResult.status_Code == 200) {
                    if (dataResult.status == 0) {
                        $(el).css("background", "red").text("InActive");
                    }
                    if (dataResult.status == 1) {
                        $(el).css("background", "green").text("Active");
                    }
                }
            },
        });
    });
    $("#delete").click(function() {
        var post_arr = [];
        $("#viewtable input[type=checkbox]").each(function() {
            if (jQuery(this).is(":checked")) {
                //alert("hi");
                var id = this.id;
                var splitid = id.split("_");
                var pid = splitid[1];
                post_arr.push(pid);
            }
        });
        if (post_arr.length > 0) {
            var isDelete = confirm("Do you really want to delete records?");
            if (isDelete == true) {
                // AJAX Request
                $.ajax({
                    url: "responce.php",
                    type: "POST",
                    data: { method: "multipledelete", id: post_arr },
                    success: function(response) {
                        $.each(post_arr, function(i, l) {
                            $("#tr_" + l).remove();
                        });
                    },
                });
            }
        }
    });
});

function checkIfFileLoaded(fileName) {
    $.ajax({
        url: "images/" + fileName,
        type: "HEAD",
        error: function() {
            $(".updateImage").attr("src", "download.png");
        },
        success: function() {
            $(".updateImage").attr("src", "images/" + fileName);
        },
    });
}