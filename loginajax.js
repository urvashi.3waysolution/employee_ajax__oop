$(document).ready(function() {
    $("#submitLogin").click(function(event) {
        event.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();

        if (email != "" && password != "") {
            $.ajax({
                url: "responce.php",
                type: "POST",
                data: { method: "login", email: email, password: password },
                cache: false,
                success: function($responce) {
                    var responce = JSON.parse($responce);

                    if (responce.status_Code == 200) {
                        $("#error").show();
                        $('#error').text(' Login Successfully!!');
                        window.location.href = 'index.php';
                    } else {
                        $("#error").show();
                        $('#error').text('Invalid EmailId or Password !');
                    }
                },
            });
        } else {
            $("#error").show();
            $('#error').text('Plase Fill All the Fields !! !');
        }
    });
});